﻿using Xamarin.Forms;
using Pleioapp.iOS;
using Xamarin.Auth;
using System.Linq;

[assembly: Dependency(typeof(CredentialStore))]
namespace Pleioapp.iOS
{
	public class CredentialStore : ITokenStore
	{
	    private const string Context = "Pleio2";
	    readonly AccountStore _store;

		public CredentialStore() {
			_store = AccountStore.Create();
			var records = _store.FindAccountsForService (Context);
		}

		public void saveToken(AuthToken token) {
			clearTokens ();

			var newAccount = new Account ();
			newAccount.Username = token.accessToken;
			if (token.expiresIn != null) { newAccount.Properties.Add ("exires_in", token.expiresIn); }
			if (token.tokenType != null) { newAccount.Properties.Add ("token_type", token.tokenType); }
			if (token.scope != null) { newAccount.Properties.Add ("scope", token.scope); }
			if (token.refreshToken != null) { newAccount.Properties.Add ("refresh_token", token.refreshToken); }
		    if (token.mainSiteName != null) { newAccount.Properties.Add("mainSiteName", token.mainSiteName); }
		    if (token.mainSiteUrl != null) { newAccount.Properties.Add("mainSiteUrl", token.mainSiteUrl); }
            _store.Save (newAccount, Context);
		}

		public AuthToken getToken() {
			var account = _store.FindAccountsForService (Context).FirstOrDefault ();

			if (account == null) {
				return null;
			} else {
				var token = new AuthToken();
				token.accessToken = account.Username;

				if (account.Properties.ContainsKey("expires_in")) { token.expiresIn = account.Properties ["expires_in"]; }
				if (account.Properties.ContainsKey("token_type")) { token.tokenType = account.Properties ["token_type"]; }
				if (account.Properties.ContainsKey("scope")) { token.scope = account.Properties ["scope"]; }
				if (account.Properties.ContainsKey("refresh_token")) { token.refreshToken = account.Properties ["refresh_token"]; }
                if (account.Properties.ContainsKey("mainSiteName")) { token.mainSiteName = account.Properties["mainSiteName"]; }
                if (account.Properties.ContainsKey("mainSiteUrl")) { token.mainSiteUrl = account.Properties["mainSiteUrl"]; }
                return token;
			}
		}

		public void clearTokens() {
			var accounts = AccountStore.Create().FindAccountsForService(Context).ToList();
			accounts.ForEach(account => AccountStore.Create().Delete(account, Context));
		}
	}
}

