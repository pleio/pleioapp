﻿using System;
using Xamarin.Forms;
using Pleioapp.iOS;
using Foundation;

[assembly: Dependency(typeof(BrowserService))]
namespace Pleioapp.iOS
{

	public class BrowserService : IBrowserService
	{
		public void OpenUrl(string url) {
			UIKit.UIApplication.SharedApplication.OpenUrl (new NSUrl (url));
		}
	}
}

