﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace Pleioapp.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            // Get current device token
            var deviceTokenDescription = deviceToken.Description;
            if (!string.IsNullOrWhiteSpace(deviceTokenDescription))
            {
                deviceTokenDescription = deviceTokenDescription.Trim('<').Trim('>').Replace(" ", "");
            }

            // Get previous device token
            var oldDeviceToken = NSUserDefaults.StandardUserDefaults.StringForKey("PushDeviceToken");

            // Has the token changed?
            if (string.IsNullOrEmpty(oldDeviceToken) || !oldDeviceToken.Equals(deviceTokenDescription))
            {
                var service = new PushService();
                service.SaveToken(deviceTokenDescription);
                service.RegisterToken();
            }
        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            System.Diagnostics.Debug.WriteLine("A problem occured during push notification registration " +
                                               error.LocalizedDescription);
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            // @todo: convert NSDictionary to normal Dictionary and pass variables
            var data = new Dictionary<string, string>();

            var app = (App)Xamarin.Forms.Application.Current;
            app.PushService.ProcessPushNotification(data);
        }
    }
}
