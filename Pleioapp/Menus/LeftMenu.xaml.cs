﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Linq;

namespace Pleioapp
{
    public partial class LeftMenu
    {
        public readonly ObservableCollection<Group> Groups = new ObservableCollection<Group>();
        public Site CurrentSite;

        private readonly App _app = (App) Application.Current;

        public ListView Menu;
        public ListView SiteMenu;

        public LeftMenu()
        {
            InitializeComponent();

            Menu = GroupsListView;
            BindingContext = _app.MainSite;

            GroupsListView.ItemsSource = Groups;

            CouldNotLoad.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () => { await GetGroups(); }),
                NumberOfTapsRequired = 1
            });

            LogoutButton.Clicked += (s, e) => { OnLogout(); };

            MessagingCenter.Subscribe<Application>(Application.Current, "refresh_menu",
                async (sender) => { await GetGroups(); });
        }


        public async void OnLogout()
        {
            MessagingCenter.Send(Application.Current, "logout");

            await _app.PushService.DeregisterToken();

            _app.CurrentGroup = null;
            _app.AuthToken = null;
            Groups.Clear();

            var store = DependencyService.Get<ITokenStore>();
            store.clearTokens();

            MessagingCenter.Send(Application.Current, "login");
            MessagingCenter.Send(Application.Current, "refresh_menu");
            MessagingCenter.Send(Application.Current, "select_group");
        }

        public async Task GetGroups()
        {
            if (_app.MainSite == null)
            {
                Groups.Clear();
                return;
            }

            CouldNotLoad.IsVisible = false;
            ActivityIndicator.IsVisible = true;

            try
            {
                var groupsAtService = await _app.WebService.GetGroups();

                foreach (var group in groupsAtService)
                {
                    if (!Groups.Contains(group))
                    {
                        Groups.Add(group);
                    }
                    else
                    {
                        Groups.First(g => g.guid == group.guid).activitiesUnreadCount = group.activitiesUnreadCount;
                    }
                }
                var groupsToRemove = new List<Group>();
                foreach (var group in Groups)
                {
                    if (!groupsAtService.Contains(group))
                    {
                        groupsToRemove.Add(group);
                    }
                }

                foreach (var group in groupsToRemove)
                {
                    Groups.Remove(group);
                }
            }
            catch (Exception e)
            {
                CouldNotLoad.IsVisible = true;
                System.Diagnostics.Debug.WriteLine("Catched exception " + e);
            }

            ActivityIndicator.IsVisible = false;
        }
    }
}