﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace Pleioapp
{
    public partial class ActivityPage : ContentPage
    {
        private readonly ObservableCollection<Activity> _activities = new ObservableCollection<Activity>();
        private readonly App _app = (App) Application.Current;
        private Group _group;

        public ActivityPage()
        {
            InitializeComponent();
            ActivityListView.ItemsSource = _activities;

            ActivityListView.ItemSelected += (sender, e) =>
            {
                var activity = e.SelectedItem as Activity;
                if (activity?.targetObject.url != null)
                {
                    _app.SsoService.OpenUrl(activity.targetObject.url);
                }
            };

            CouldNotLoad.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => { Reload(); }),
                NumberOfTapsRequired = 1
            });

            MessagingCenter.Subscribe<Application>(Application.Current, "select_group", async (sender) =>
            {
                var app = (App) Application.Current;
                setGroup(app.CurrentGroup);
            });

            MessagingCenter.Subscribe<Application>(Application.Current, "logout",
                async (sender) => { SelectLeft.IsVisible = true; });
        }

        public async void Reload()
        {
            _activities.Clear();
            CouldNotLoad.IsVisible = false;
            NoItems.IsVisible = false;
            ActivityIndicator.IsVisible = true;

            try
            {
                var service = _app.WebService;
                var webActivities = await service.GetActivities(_group);
                foreach (var activity in webActivities)
                {
                    _activities.Add(activity);
                }

                if (webActivities.Count == 0)
                {
                    NoItems.IsVisible = true;
                }
            }
            catch (Exception e)
            {
                CouldNotLoad.IsVisible = true;
                System.Diagnostics.Debug.WriteLine("Catched exception " + e);
            }

            ActivityIndicator.IsVisible = false;
        }

        public async void setGroup(Group group)
        {
            _group = group;
            if (group == null)
            {
                _activities.Clear();
                return;
            }

            SelectLeft.IsVisible = false;
            CouldNotLoad.IsVisible = false;
            Reload();

            if (group.activitiesUnreadCount > 0)
            {
                _app.CurrentGroup.MarkAsRead();
                var service = _app.WebService;
                await service.MarkGroupAsRead(_group);
            }
        }
    }
}