﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace Pleioapp
{
	public partial class MemberPage : ContentPage
	{
	    private readonly ObservableCollection<User> _members = new ObservableCollection<User>();
	    private readonly App _app = (App) Application.Current;
	    private Group _group;

	    public MemberPage ()
		{
			InitializeComponent ();
			MemberListView.ItemsSource = _members;

			MemberListView.ItemSelected += (sender, e) => {
				var member = e.SelectedItem as User;
				if (member?.url != null) {
					_app.SsoService.OpenUrl(member.url);
				}
			};
				
			CouldNotLoad.GestureRecognizers.Add (new TapGestureRecognizer {
				Command = new Command (() => {
					Reload();
				}),
				NumberOfTapsRequired = 1
			});

			MessagingCenter.Subscribe<Application> (Application.Current, "select_group", async(sender) => {
				var app = (App)Application.Current;
				SetGroup(app.CurrentGroup);
			});
		}

		public async void Reload()
		{
			CouldNotLoad.IsVisible = false;
			NoItems.IsVisible = false;
			_members.Clear ();

			var app = (App)Application.Current;
			var service = app.WebService;

			try {
				var webMembers = await service.GetMembers (_group);

				foreach (var member in webMembers) {
					_members.Add (member);
				}

				if (_members.Count  == 0) {
					NoItems.IsVisible = true;
				}
			} catch (Exception e) {
				CouldNotLoad.IsVisible = true;
				System.Diagnostics.Debug.WriteLine ("Catched exception " + e);
			}

		}

	    public void SetGroup(Group group)
		{
			_group = group;
			if (group == null) {
				_members.Clear ();
				return;
			}
				
			Reload ();
		}
	}
}

