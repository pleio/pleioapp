﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace Pleioapp
{
	public partial class EventPage : ContentPage
	{
	    private readonly ObservableCollection<Event> _events = new ObservableCollection<Event>();
	    private readonly App _app = (App) Application.Current;
	    private Group _group;

		public EventPage ()
		{
			InitializeComponent ();
			EventListView.ItemsSource = _events;

			EventListView.ItemSelected += (sender, e) => {
				var selectedEvent = e.SelectedItem as Event;
				if (selectedEvent?.url != null) {
					_app.SsoService.OpenUrl(selectedEvent.url);
				}
			};

			CouldNotLoad.GestureRecognizers.Add (new TapGestureRecognizer {
				Command = new Command (() => {
					Reload();
				}),
				NumberOfTapsRequired = 1
			});

			MessagingCenter.Subscribe<Application> (Application.Current, "select_group", async(sender) => {
				var app = (App)Application.Current;
				setGroup(app.CurrentGroup);
			});
		}

		public async void Reload()
		{
			CouldNotLoad.IsVisible = false;
			NoItems.IsVisible = false;
			_events.Clear ();

			var app = (App)Application.Current;
			var service = app.WebService;

			try {
				var webEvents = await service.GetEvents (_group);

				foreach (var e in webEvents) {
					_events.Add (e);
				}

				if (_events.Count == 0) {
					NoItems.IsVisible = true;
				}
			} catch (Exception e) {
				CouldNotLoad.IsVisible = true;
				System.Diagnostics.Debug.WriteLine ("Catched exception " + e);
			}
		}

		public async void setGroup(Group group)
		{
			_group = group;
			if (group == null) {
				_events.Clear ();
				return;
			}

			Reload ();
		}

	}
}

