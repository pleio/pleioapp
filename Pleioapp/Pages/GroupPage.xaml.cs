﻿using Xamarin.Forms;

namespace Pleioapp
{
	public partial class GroupPage : TabbedPage
	{
	    public GroupPage ()
		{
		    InitializeComponent ();

			var activityPage = new ActivityPage ();
			var eventPage = new EventPage ();
			var memberPage = new MemberPage ();
			var filePage = new FilePage();

			Children.Add (activityPage);
			Children.Add (eventPage);
			Children.Add (memberPage);
			Children.Add (filePage);

			MessagingCenter.Subscribe<Application> (Application.Current, "select_group", async(sender) => {
				var app = (App)Application.Current;
				BindingContext = app.CurrentGroup;
			});
		}
	}
}

