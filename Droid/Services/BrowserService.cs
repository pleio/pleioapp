﻿using System;
using Xamarin.Forms;
using Pleioapp.Droid;

[assembly: Dependency(typeof(BrowserService))]

namespace Pleioapp.Droid
{
    public class BrowserService : IBrowserService
    {
        public void OpenUrl(string url)
        {
            Device.OpenUri(new Uri(url));
        }
    }
}