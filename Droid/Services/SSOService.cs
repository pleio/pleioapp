﻿using System;
using Xamarin.Forms;
using Pleioapp.Droid;
using System.Threading.Tasks;

[assembly: Dependency(typeof(SSOService))]
namespace Pleioapp.Droid
{
	public class SSOService : ISSOService
	{
	    private readonly WebService _webService;
        private SSOToken _loginToken;
	    private bool _loading;
	    private double _tokenExpiry;
	    private double _loginExpiry;
	    private readonly App _app;

		public SSOService() {
			_app = (App)Application.Current;
			_webService = _app.WebService;

			MessagingCenter.Subscribe<Application> (Application.Current, "login_succesful", async(sender) => {
				await LoadToken();
			});

			MessagingCenter.Subscribe<Application> (Application.Current, "logout", (sender) => {
				Expire();
			});

			MessagingCenter.Subscribe<Application> (Application.Current, "select_site", async(sender) => {
				Expire();
				await LoadToken();
			});
		}

		public void Expire() {
			_loginToken = null;
			_tokenExpiry = 0;
		}

		public async Task<bool> LoadToken() {
			System.Diagnostics.Debug.WriteLine ("[SSO] requesting SSO token");
			_loginToken = await _webService.GenerateToken ();
			if (_loginToken != null) {
				_tokenExpiry = UnixTimestamp () + _loginToken.expiry;
			}
			return true;
		}

		private double UnixTimestamp() {
			return DateTime.UtcNow.Subtract (new DateTime (1970, 1, 1)).TotalSeconds;
		}

		public async void OpenUrl(string url) {
			string loadUrl;

			if (_loading) {
				return; // prevent double click..
			} else {
				_loading = true;
			}

			if (UnixTimestamp() > _loginExpiry && UnixTimestamp() > _tokenExpiry) {
				await LoadToken ();
			}

			if (_loginToken != null) {
				loadUrl = _app.MainSite.url + "api/users/me/login_token?user_guid=" + _loginToken.userGuid + "&token=" + _loginToken.token + "&redirect_url=" + url;
				_loginToken = null;

				_loginExpiry = UnixTimestamp () + 60 * 60;
			} else {
				loadUrl = url; // could not retrieve token
			}

			_loading = false;

			System.Diagnostics.Debug.WriteLine ("[SSO] opening: " + loadUrl);
			Device.OpenUri (new Uri (loadUrl));
		}
	}

}

